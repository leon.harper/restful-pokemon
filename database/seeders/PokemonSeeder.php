<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PokemonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $listRequest = Http::get('https://pokeapi.co/api/v2/pokemon?limit=151');
        
        $pokemonlist = json_decode($listRequest->body(), true);
        foreach ($pokemonlist['results'] as $item) {
            $itemRequest = Http::get($item['url']);
            $pokemon = json_decode($itemRequest->body(), true);

            echo 'inserting ' . $pokemon['name'] . PHP_EOL;

            try {
                DB::table('pokemon')->insert([
                    'id' => $pokemon['id'],
                    'name' => $pokemon['name'],
                    'url' => $item['url'],
                    'base_experience' => $pokemon['base_experience'],
                    'height' => $pokemon['height'],
                    'is_default' => $pokemon['is_default'],
                    'sprite_url' => $pokemon['sprites']['front_default'],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'  => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}
