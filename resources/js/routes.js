import PokeList from './views/components/pokeList.vue';
import Pokemon from './views/components/pokemon.vue';

export const routes = [
    {
        name: 'pokelist',
        path: '/',
        component: PokeList
    },
    {
        name: 'pokemon',
        path: '/pokemon/:id',
        component: Pokemon
    }
];