import Vue from 'vue';
import axios from 'axios';
import VueRouter from 'vue-router';

//Main pages
import App from './views/app.vue';
import { routes } from './routes';
import VueFormulate from '@braid/vue-formulate';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: routes
});
 
Vue.use(VueFormulate);

// Vue.use(VueFormulate, {
//     classes: {
//         outer: 'mb-4',
//         input: 'form-input w-full mt-2 rounded-md focus:border-indigo-600',
//         label: 'text-gray-700',
//         help: 'text-xs mb-1 text-gray-600',
//         error: 'text-red-700 text-xs mb-1'
//       }
// });

const app = new Vue({
    el: '#app',
    router: router,
    components: { App }
});
