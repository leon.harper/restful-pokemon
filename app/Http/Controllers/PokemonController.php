<?php

namespace App\Http\Controllers;

use App\Models\Pokemon;
use Illuminate\Http\Request;
use App\Http\Resources\PokemonResource;

class PokemonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return paginated pokemon data
        $perPage = $request->query('perPage') ?? 15;
        $term = $request->query('search') ?? false;
        if ($term) {
            $pokemon = Pokemon::latest()
                ->where('id', 'like', '%' . $term .'%')
                ->orWhere('name', 'like', '%' . $term .'%')
                ->orWhere('url', 'like', '%' . $term .'%') 
                ->orWhere('base_experience', 'like', '%' . $term .'%') 
                ->orWhere('height', 'like', '%' . $term .'%') 
                ->orWhere('id', 'like', '%' . $term .'%')
                ->paginate($perPage);
            return PokemonResource::collection($pokemon);
        }

        $pokemon = Pokemon::paginate($perPage);
        return PokemonResource::collection($pokemon);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pokemon::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->name = $request->name;
        $pokemon->height = $request->height;
        $pokemon->base_experience = $request->base_experience;
        $pokemon->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->delete();
    }
}
