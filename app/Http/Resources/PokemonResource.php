<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Carbon\Carbon;

class PokemonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $datetime = new Carbon($this->updated_at);

        return [
            "Sprite" => $this->sprite_url,
            "Name" => $this->name,
            "Base Experience" => $this->base_experience,
            "Height" => $this->height,
            "Default" => $this->is_default,
            "Updated" => $datetime->format('d-m-Y'),
            'Profile' => 'pokemon/' . $this->id
        ];

    }
}
