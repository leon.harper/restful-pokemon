A small project to demonstrate seeding a database via an api endpoint and presenting the data in a table. Developed locally using 
laravel sail.

A live version can be found at https://leonharper.uk/

To run on a local environment please clone the project and then the following. When asked please choose mariadb as the database.
```
composer require laravel/sail --dev
cp .env.example .env
php artisan sail:install
```
add the sail alias
```
alias sail='bash vendor/bin/sail
```
bring up sail
```
./vendor/bin/sail up
```

migrate the database

```
sail php artisan migrate
```

To seed the pokemon data please run PokemonSeeder

```
sail php artisan db:seed --class=PokemonSeeder
```

and then install the vue application 
```
sail npm install
sail npm run dev
```

once complete the application will be available at localhost

routes are as follows. Store has not been implemented.

```
+--------+-----------+-----------------------+-----------------+------------------------------------------------+------------+
| Domain | Method    | URI                   | Name            | Action                                         | Middleware |
+--------+-----------+-----------------------+-----------------+------------------------------------------------+------------+
|        | GET|HEAD  | api/pokemon           | pokemon.index   | App\Http\Controllers\PokemonController@index   | api        |
|        | POST      | api/pokemon           | pokemon.store   | App\Http\Controllers\PokemonController@store   | api        |
|        | GET|HEAD  | api/pokemon/{pokemon} | pokemon.show    | App\Http\Controllers\PokemonController@show    | api        |
|        | PUT|PATCH | api/pokemon/{pokemon} | pokemon.update  | App\Http\Controllers\PokemonController@update  | api        |
|        | DELETE    | api/pokemon/{pokemon} | pokemon.destroy | App\Http\Controllers\PokemonController@destroy | api        |
|        | GET|HEAD  | api/user              |                 | Closure                                        | api        |
|        |           |                       |                 |                                                | auth:api   |
|        | GET|HEAD  | {any}                 |                 | App\Http\Controllers\PagesController@index     | web        |
+--------+-----------+-----------------------+-----------------+------------------------------------------------+------------+
```
